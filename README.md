# What is List?
List is a website directory database full of websites added by users and approved by a staff member.

# Installation
If you want to install the web directory you will need to download the source code.
- Grab the source code from this repository.
- Upload and extract the zip file to the root of your webserver where it can be accessed by anyone.
- No need to configure a database (in the process of making this available I am planning to update the source code to use MySQL or PostgreSQL)

# How to review websites added in the database?
You can modify the database.json file and use CTRL+F or Search for the word "pending" and if you believe the site is safe then mark it as "safe".